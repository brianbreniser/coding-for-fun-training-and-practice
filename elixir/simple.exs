IO.puts("Hello world from elixir")

defmodule Recursion do
  @my_data 100

  def p() do
    IO.puts(@my_data)
  end

  def sum_list(list) do
    sum_list(list, 0)
  end

  def sum_list([head | tail], acc) do
    sum_list(tail, acc + head)
  end

  def sum_list([], acc) do
    acc
  end
end

defmodule Geo do
  def area_loop do
    receive do
      {:rectangle, w, h} ->
        IO.puts("Area = #{w * h}")
        area_loop()

      {:circle, r} ->
        IO.puts("Area = #{3.14 * r * r}")
        area_loop()
    end
  end
end

IO.puts(Recursion.sum_list([1, 2, 3]))

Recursion.p()

Range.new(1, 10)
|> Enum.map(fn x -> x * x end)
|> Enum.filter(fn x -> rem(x, 2) == 0 end)
|> IO.inspect()

pid = spawn(fn -> Geo.area_loop() end)

send(pid, {:rectangle, 2, 3})
send(pid, {:circle, 5})

IO.inspect(self())

{:ok, my_agent} = Agent.start_link(fn -> ["red", "green"] end)

ret = Agent.get(my_agent, fn colors -> colors end)
IO.inspect(ret)

ret = Agent.update(my_agent, fn colors -> ["blue" | colors] end)
IO.inspect(ret)
