package breniser;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Didn't crash!" );
        App app = new App();
        int re = app.example();

        List<int> = List.of(1, 2, 3, 4);
    }

    public int example() {
        abstract class Nested {
            public abstract int doit();
        }

        class Dwiw extends Nested {
            public int doit() {
                return 1;
            }
        }

        Dwiw dwiw = new Dwiw();

        int result = dwiw.doit();

        return result;
    }

    public void One() {
        class Initme {
            int f1;
            int f2;
            String f3;

            Initme() {

            }

            Initme(int f1, int f2, String f3) {
                this.f1 = f1;
                this.f2 = f2;
                this.f3 = f3;
            }
        }

        Initme initme = new Initme();
        Initme initme1 = new Initme(1, 2, "hello");
    }
}

