package org.acme.getting.started;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Random;

@Path("/hello")
public class GreetingResource {

    @Inject
    GreetingService greetingService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/greeting/{name}")
    public String greeting(@PathParam String name) {
        return greetingService.greeting(name);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/insult/{name}")
    public String insult(@PathParam String name) {
        Random random = new Random(System.currentTimeMillis());

        int n = random.nextInt(5);
        String insult = "";
        switch(n) {
            case 0: insult = "Fuck off %s"; break;
            case 1: insult = "Piss off %s"; break;
            case 2: insult = "Go fuck yourself %s"; break;
            case 3: insult = "Die and burn in a fire %s"; break;
            case 4: insult = "%s is suck a piece of shit"; break;
        }

        return String.format(insult, name);
    }
}