package org.acme;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class GreetingResourceTest {

    @Test
    public void quicktest(){
        given()
            .when().get("/greet/testsr")
            .then()
            .statusCode(200)
            .body(is("It worked"));
    }

    @Test
    public void testGreetHelloEndpoint() {
        given()
            .when().get("/greet/hello")
            .then()
            .statusCode(200)
            .body(is("Hello from RESTEasy Reactive"));
    }

    @Test
    public void testGreetByeEndpoint() {
        given()
            .when().get("/greet/bye/felecia")
            .then()
            .statusCode(200)
            .body(is("BYE felecia null"));
    }

    @Test
    public void testGreetByeEndpointWithArgs() {
        given()
            .when().get("/greet/bye/felecia?other=bob")
            .then()
            .statusCode(200)
            .body(is("BYE felecia bob"));
    }
}
