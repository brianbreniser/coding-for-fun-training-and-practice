package org.acme;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

import static org.hamcrest.CoreMatchers.is;
import static io.restassured.RestAssured.given;

@QuarkusTest
class ExpenseResourceTest{

    @Test
    public void quickTest(){
        given()
            .when().get("/expense/test")
            .then()
            .statusCode(200)
            .body(is("Works"));
    }

    @Test
    public void testRandom(){
        given()
            .when().get("/expense/random")
            .then()
            .statusCode(200)
            .body(is("TODO"));
    }

}

