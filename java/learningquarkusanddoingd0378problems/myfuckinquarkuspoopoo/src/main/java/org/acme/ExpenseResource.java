package org.acme;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/expense")
public class ExpenseResource {
    @Inject
    ExpenseService expenseService;

    @GET
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    public String testit() {
        return "Works";
    }

    @GET
    @Path("/random")
    @Produces(MediaType.APPLICATION_JSON)
    public String makeRandom() {
        Expense e = Expense.makeWithRandomId();

        return "TODO";
    }

    @GET
    public Set<Expense> list() {
        return new HashSet<Expense>();
    }

    @POST
    public Expense create() {
        return new Expense();
    }

    @PUT
    public void update(Expense expense) {

    }

    @DELETE
    public Set<Expense> delete(@PathParam("uuid") UUID uuid) {
        return new HashSet<Expense>();
    }
}
