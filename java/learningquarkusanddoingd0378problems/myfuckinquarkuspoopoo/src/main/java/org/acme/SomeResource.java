package org.acme;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SomeResource {
    public String doit() {
        System.out.println("It worked");
        return "It worked";
    }
}

