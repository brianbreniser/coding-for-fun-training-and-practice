package org.acme;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/greet")
public class GreetingResource {
    @Inject
    SomeResource sr;

    @GET
    @Path("/testsr")
    @Produces(MediaType.TEXT_PLAIN)
    public String usesr() {
        String message = sr.doit();
        return message;
    }

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello from RESTEasy Reactive";
    }

    @GET
    @Path("/bye/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response bye(
            @PathParam("name") String name,
            @QueryParam("other") String othername
        ) {
        String response = String.format("BYE %s %s", name, othername);
        return Response.status(200).entity(response).build();
    }
}
