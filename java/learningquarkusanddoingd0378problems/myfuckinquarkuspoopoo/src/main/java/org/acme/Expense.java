package org.acme;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.json.bind.annotation.JsonbCreator;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

class Expense extends PanacheEntity {
    UUID id;
    String name;
    String description;

    @JsonbCreator
    public static Expense makeWithRandomId() {
        Expense e = new Expense();

        e.id = UUID.randomUUID();
        e.name = "New name";
        e.description = "New description";

        return e;
    }

    public static List<Expense> search(String query) {
        String queryNonNull = Objects.requireNonNullElse(query, "UNKNOWN");
        String queryUpper = queryNonNull.toUpperCase() + "%";
        //TODO: finish
        return new ArrayList<>();
    }
}

