#!/usr/bin/python3

def doit(x):
    return sum([ i for i in range(1, x) if (x % i) == 0 ]) == x

for i in range(1, 18446700000000000000):
    if doit(i):
        print(i)

