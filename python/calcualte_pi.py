#!/usr/bin/python3
Black = "\u001b[30m"
Red = "\u001b[31m"
Green = "\u001b[32m"
Yellow = "\u001b[33m"
Blue = "\u001b[34m"
Magenta = "\u001b[35m"
Cyan = "\u001b[36m"
White = "\u001b[37m"
Reset = "\u001b[0m"

def iterate(pi, s):
    return pi + (4/((s)*(s+1)*(s+2))) - (4/((s+2)*(s+3)*(s+4)))

real_pi = "3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798"
pi = 3
s = 2

for i in range(1, 9999999):
    done = False
    pi = iterate(pi, s)
    s = s+4

    if (i%100000) == 0:
        done = True # will set to False if we are not done
        compare = str(pi)

        print()
        print("Let's examine:")
        print(real_pi)
        print(compare)

        for i in range(0, len(compare)):
            if compare[i] != real_pi[i]:
                print("x", end="")
                done = False
            else:
                print(" ", end="")

    if done:
        print("Iterations: {}".format(i))
        print()
        break

