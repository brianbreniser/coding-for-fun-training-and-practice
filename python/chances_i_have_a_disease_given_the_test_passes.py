#!/usr/bin/python3

import unittest
import sys

# relevant comments:
#
#       Disease+   Disease-
#
# Test+     A         B
# Test-     C         D
#
# A is true  positive (Have disease)
# B is false positive (Don't)
# C is false negative (Have disease)
# D is true  negative (Don't)
#
# Sensitivity = A/(A+C) (Accuracy given you have disease)
# Specificity = D/(D+B) (Accuracy given you don't have disease)
#
# Chance to test positive  = (A+B)/(A+B+C+D)
# Chance to test negative  = (C+D)/(A+B+C+D)
# Chance positive test is accurate = A/(A+B)
# Chance negative test is accurate = D/(C+D)
#
# H = have disease
# E = test positive
# nE = don't have disease
# nH = test negative
#
# p(H|E) probability of 
#
#
#
#
# rr is real rate (percent of people that have it)
# hr is health rate (percent of healthy people (1-rr))
#

def probability_of_having_disease_given_you_tested_positive(pH, pEH, pnH, pEnH):
    top = pH * pEH
    bottom1 = top
    bottom2 = pnH*pEnH
    return top/(bottom1+bottom2)

def probability_of_not_having_disease_given_tested_negative(pnH, pnEnH, pH, pnEH):
    top = pnH * pnEnH
    bottom1 = top
    bottom2 = pH*pnEH
    return top/(bottom1+bottom2)

def chances(prob, sensitivity, specificity):
    pEH = sensitivity
    pEnH = 1-sensitivity
    pnEnH = specificity
    pnEH = 1-specificity
    pH = prob
    pnH = 1-prob

    first = probability_of_having_disease_given_you_tested_positive(pH, pEH, pnH, pEnH)
    second = probability_of_not_having_disease_given_tested_negative(pnH, pnEnH, pH, pnEH)

    return first, second


class TestChances(unittest.TestCase):

    #def test_first(self):
        # self.assertRaises(anExceptionErrorType):
        #   somethingThatRaisesThatErrorType()
        # self.assertEqual(1,1)
        # self.assertTrue(True)
        # self.assertFalse(False)

    def test_no_one_has_it(self):
        self.assertEqual((0, 1), chances(0.00, .92, .85))
        self.assertEqual((0, 1), chances(0.00, .80, .80))
        self.assertEqual((0, 1), chances(0.00, .10, .10))
        self.assertEqual((0, 1), chances(0.00, .99, .99))

    def test_everyone_has_it(self):
        self.assertEqual((1, 0), chances(1, .92, .85))
        self.assertEqual((1, 0), chances(1, .90, .90))
        self.assertEqual((1, 0), chances(1, .10, .10))
        self.assertEqual((1, 0), chances(1, .99, .99))

    def test_coin_toss(self):
        self.assertEqual((0.50, 0.50), chances(0.50, 0.50, 0.50))

    def test_half_population_has_then_tests_are_exact(self):
        self.assertEqual((0.90, 0.90), chances(0.50, 0.90, 0.90))
        self.assertEqual((0.96, 0.85), chances(0.50, 0.96, 0.85))
        self.assertEqual((0.10, 0.72), chances(0.50, 0.10, 0.72))
        self.assertEqual((0.25, 0.18), chances(0.50, 0.25, 0.18))

    def test_experiment(self):
        print()
        print()
        print(chances((0.09), .99, 0))
        print()

def print_probabilities(prob, sens, spec, number):
    print("Pass no: " + str(number))
    print("Objective possibility: {:.2f}, sensitivity: {:.2f}, specificity: {:.2f}".format(prob, sens, spec))
    chance = chances(prob, sens, spec)
    print("Chances a pass is correct: {:.2} | Chance a false is correct: {:.2}".format(chance[0], chance[1]))
    print()
    return chance


if __name__ == "__main__":

    print("===============================================================")
    print("John hopkins cases just united States")
    print("===============================================================")
    prob = 5336918/328239523
    sens = 938/1000
    spec = 956/1000

    new_chance = print_probabilities(prob, sens, spec, 1)
    new_chance = print_probabilities(new_chance[0], sens, spec, 2)
    new_chance = print_probabilities(new_chance[0], sens, spec, 3)


    print("===============================================================")
    print("John hopkins cases (Order of magnitude off) just united States")
    print("===============================================================")
    prob = 53369180/328239523
    sens = 938/1000
    spec = 956/1000

    new_chance = print_probabilities(prob, sens, spec, 1)
    new_chance = print_probabilities(new_chance[0], sens, spec, 2)
    new_chance = print_probabilities(new_chance[0], sens, spec, 3)


    print("===============================================================")
    print("John hopkins cases worldwide")
    print("===============================================================")
    prob = 21285271/7000000000
    sens = 938/1000
    spec = 956/1000

    new_chance = print_probabilities(prob, sens, spec, 1)
    new_chance = print_probabilities(new_chance[0], sens, spec, 2)
    new_chance = print_probabilities(new_chance[0], sens, spec, 3)


    print("===============================================================")
    print("John hopkins (Order of magnitude off) cases worldwide")
    print("===============================================================")
    prob = 212852710/7000000000
    sens = 938/1000
    spec = 956/1000

    new_chance = print_probabilities(prob, sens, spec, 1)
    new_chance = print_probabilities(new_chance[0], sens, spec, 2)
    new_chance = print_probabilities(new_chance[0], sens, spec, 3)

    sys.exit(0)
