package main

// =========================== Imports of course

import (
	//"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// =========================== Server Components

func hello(w http.ResponseWriter, req *http.Request) {
    log.Println("/hello: request:", req.Header, req.Host, req.Proto, req.RequestURI)
    log.Printf("%s%s Headers -> %s\n", req.Host, req.RequestURI, req.Header)
    fmt.Fprintf(w, "hello\n")
}

func headers(w http.ResponseWriter, req *http.Request) {
    log.Println("/headers: request:", req)
    for name, headers := range req.Header {
        for _, h := range headers {
            fmt.Fprintf(w, "%v: %v", name, h)
        }
    }
}

func echo(w http.ResponseWriter, req *http.Request) {

    defer req.Body.Close()
    b, _ := ioutil.ReadAll(req.Body)
    d := string(b)

    log.Println("/echo: request:", d)

    fmt.Fprintf(w, "%s\n", d)
}

// =========================== Server

func main() {
    port := "42069"
    fmt.Println("Starting, searving on port:", port)

    http.HandleFunc("/hello", hello)
    http.HandleFunc("/headers", headers)
    http.HandleFunc("/echo", echo)

    http.ListenAndServe(port, nil)
}

// =========================== Person

type NotFoundError struct {
    error
}

func (n NotFoundError) isNotFound(){}

func NotFound(e error) bool {
    if _, ok := e.(NotFoundError); ok {
        return true
    }
    return false
}

type Person struct {
    Id uint64
    Name string
    Age int
}

func (p *Person) Validate() error {
    errorString := ""
    errorList := []string{}

    if p.Id == 0 {
        errorList = append(errorList, "id field should not be be 0")
    }

    if len(p.Name) == 0 {
        errorList = append(errorList, "name should not be empty")
    }

    if p.Age == 0 {
        errorList = append(errorList, "age must be filled")
    }

    if len(errorList) != 0 {
        for _, i := range errorList {
            errorString = errorString + i + " And "
        }
        errorString = "Found errors while validating a Person: " + errorString
        return errors.New(errorString[0:len(errorString) - 5])
    }

    return nil
}

// Database Interface

type abstractdb interface {
    Get(id uint64) (*Person, error)
    Put(p *Person) (error)
    Remove(id uint64) (*Person, error)
    Search(id uint64) (*Person, error)
}

// =========================== In memory storage

type InMemoryStorage struct {
    store map[uint64]*abstractdb
}

func new() *InMemoryStorage {
    return &InMemoryStorage{store: map[uint64]*abstractdb{}}
}

