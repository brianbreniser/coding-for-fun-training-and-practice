package main

import (
	"bufio"
	"fmt"
	"net/http"
)

func print_map(m map[string][]string) {
    for i, x := range m {
        fmt.Println(i, x)
    }
}

func main() {
    fmt.Println("Staring")

    resp, err := http.Get("https://gobyexample.com")
    if err != nil {
        panic(err)
    }

    fmt.Println("Response:", resp.Status)

    fmt.Println(resp.ContentLength)
    fmt.Println(resp.Header)
    print_map(resp.Header)
    fmt.Println(resp.Request)
    fmt.Println(resp.Cookies())

    scanner := bufio.NewScanner(resp.Body)
    for i := 0; scanner.Scan() && i < 2; i++ {
        fmt.Println(scanner.Text())
    }
    if err := scanner.Err(); err != nil {
        panic(err)
    }
}
