package main

import (
    "fmt"
    "os"
    "bufio"
    "strconv"
    "sort"
)

func main() {
    fmt.Println("Looping through file")
    getfile()
}

func getfile() {
    file, err := os.Open("input.txt")
    if err != nil {
        fmt.Println("Error opening the file:", err)
    }
    defer file.Close()
    scanner := bufio.NewScanner(file)

    // Will hold all the totals of each elf
    totals := []int{}

    // Temporary accumulator
    accum := 0

    for scanner.Scan() {
        line := scanner.Text()
        if isNotEmptyLine(line) {
            // Format of file is such that if the line isn't empty, we need to accumulate the number
            num, _ := strconv.Atoi(line) // convert string to int
            accum += num
        }
        if isEmptyLine(scanner.Text()) {
            // Format of file is such that if the line is empty, we need to start a new accumulator
            totals = append(totals, accum)
            accum = 0
        }
    }


    // sort the totals
    sort.Ints(totals)

    fmt.Println(totals)
    // print only last 3 elements of slice
    topthree := totals[len(totals)-3:]
    final := 0
    for _, v := range topthree {
        final += v
    }

    fmt.Println(final)
}

func isEmptyLine(line string) bool {
    if line == "" {
        return true
    }
    return false
}

func isNotEmptyLine(line string) bool {
    if line != "" {
        return true
    }
    return false
}

