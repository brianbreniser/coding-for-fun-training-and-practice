package main

import (
	"fmt"
	"time"
)

func main() {
	// constants
	const size = 5

	// Time
	start := time.Now()

	// switch statements
	i := 2
	fmt.Print("Write ", i, " as ")
	switch i {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("two")
	case 3:
		fmt.Println("three")
	}

	// arrays

	var a [5]int
	fmt.Println("empty int array:", a)

	// for loops
	for i := 0; i < size; i++ {
		a[i] = 1
	}

	fmt.Println("non-empty int array:", a)

	b := [5]int{1, 2, 3, 4, 5}
	fmt.Println("declare and initialize:", b)

	const sz = 5

	// 2d array
	var twoD [sz][sz]int
	for i := 0; i < sz; i++ {
		for j := 0; j < sz; j++ {
			twoD[i][j] = (i * sz) + j
		}
	}
	fmt.Println("twoD array: ", twoD)

	// slices
	s := make([]string, 3)
	fmt.Println("s:", s)

	s[0] = "a"
	s[1] = "b"
	s[2] = "c"
	fmt.Println("s:", s)

	s = append(s, "d")
	fmt.Println("s:", s)

	c := make([]string, len(s))
	copy(c, s)

	s = append(s, "d")
	fmt.Println("s:", s)
	fmt.Println("c:", c)

	d := s[2:4]
	fmt.Println("d:", d)

	// multi-dimensional slices

	slicesTwoD := make([][]int, size)
	// init inner
	for i := 0; i < size; i++ {
		slicesTwoD[i] = make([]int, size)
	}
	// fill with data
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			slicesTwoD[i][j] = (i * size) + j
		}
	}
	fmt.Println("slicesTwoD:", slicesTwoD)

	fmt.Println()

	// Maps
	m := make(map[string]int)
	m["k1"] = 7
	m["k2"] = 13

	get := m["k1"]
	fmt.Println("m:", m)
	fmt.Println("get:", get)

	delete(m, "k2")
	fmt.Println("m:", m)

	n := map[string]int{"foo": 1, "bar": 2}
	fmt.Println("n:", n)

	fmt.Println()

	// range

	nums := []int{2, 3, 4}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println("sum:", sum)

	for i, num := range nums {
		if num == 3 {
			fmt.Println("index:", i)
		}
	}

	kvs := map[string]string{"a": "apple", "b": "banana"}
	for k, v := range kvs {
		fmt.Printf("%s -> %s\n", k, v)
	}

	for i, c := range "ABCxyz" {
		fmt.Println(i, c)
	}

	fmt.Println()
	// Time
	end := time.Now()
	fmt.Printf("Total time to run this program took:", end.Sub(start))

	doit()
}

func doit() {
	p := new(int)
	*p = 7
	fmt.Println("P is equal to: ", *p)
	fmt.Println("P's memory is pointint to: ", p)
	q := new(int)
	q = &p
	fmt.Println("q is: ", q)
	fmt.Println("q is pointint to: ", q)
}
