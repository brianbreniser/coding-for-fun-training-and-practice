package main

import (
	"fmt"
	"math/rand"
	"queue/queue"
)

func m() string {
	return fmt.Sprint("HI")
}

func main() {
	q := queue.NewPqueue()

	// Add some items with different priorities
	// Add 100 items with random priorities
	for i := 0; i < 100000; i++ {
		item := rand.Intn(1000)   // Random item value between 0-999
		priority := rand.Intn(10) // Random priority between 0-9
		q.Enqueue(item, priority)
	}
	// Create map to track counts
	counts := make(map[int]int)

	// Process items in priority order
	for {
		item, err := q.Dequeue()
		if err != nil {
			fmt.Println("Empty queue, done!")
			break
		}

		// Increment count for this priority
		counts[item.Priority]++
	}

	// Print counts for each priority
	fmt.Println("\nPriority Counts:")
	for priority := 9; priority >= 0; priority-- {
		if count, exists := counts[priority]; exists {
			fmt.Printf("Priority %d: %d items\n", priority, count)
		}
	}
}
