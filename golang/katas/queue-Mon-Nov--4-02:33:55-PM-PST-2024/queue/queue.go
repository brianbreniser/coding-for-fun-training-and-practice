package queue

import (
	"fmt"
)

func Doit() string {
	return fmt.Sprint("Hi")
}

type QueueItem struct {
	Priority int
	Item     int
}

func (q QueueItem) String() string {
	return fmt.Sprintf("priority:%d item:%d", q.Priority, q.Item)
}

type Pqueue struct {
	items []QueueItem
}

func NewPqueue() Pqueue {
	return Pqueue{
		items: []QueueItem{},
	}
}

func (q *Pqueue) Enqueue(item, priority int) {
	if len(q.items) == 0 {
		q.items = append(
			q.items,
			QueueItem{
				Priority: priority,
				Item:     item,
			},
		)

		return
	}

	for i, curr := range q.items {
		if priority > curr.Priority {
			new_item_slice := []QueueItem{{priority, item}}
			right_side := append(new_item_slice, q.items[i:]...)
			q.items = append(q.items[:i], right_side...)
			return
		}
	}

	// Add to end if priority is lowest
	q.items = append(
		q.items,
		QueueItem{
			Priority: priority,
			Item:     item,
		},
	)
}

func (q *Pqueue) Dequeue() (QueueItem, error) {
	if len(q.items) == 0 {
		return QueueItem{}, fmt.Errorf("No more items")
	}

	item := q.items[0]

	q.items = q.items[1:]

	return item, nil
}

func (q *Pqueue) Next() (QueueItem, error) {
	return q.Dequeue()
}
