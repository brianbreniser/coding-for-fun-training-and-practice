package queue

import (
	"fmt"
	"testing"

	"math/rand"
)

func TestQ(t *testing.T) {
	t.Run("doit", func(t *testing.T) {
		if 1 != 1 {
			t.Fail()
		}
	})
}

func TestTwo(t *testing.T) {
	t.Run("default", func(t *testing.T) {
		if 2 != 2 {
			t.Error("failed")
		}
	})
}

func TestNewPqueue(t *testing.T) {
	t.Run("create empty", func(t *testing.T) {
		pq := NewPqueue()
		if len(pq.items) != 0 {
			t.Error("expected empty queue")
		}
	})
}

func TestAdd(t *testing.T) {
	t.Run("add one item", func(t *testing.T) {
		pq := NewPqueue()
		pq.Enqueue(10, 1)
		if len(pq.items) != 1 {
			t.Error("expected 1 item in queue")
		}
		if pq.items[0].Priority != 1 {
			t.Error("expected priority 1")
		}
		if pq.items[0].Item != 10 {
			t.Error("expected item 10")
		}
	})
}

func TestAddTwo(t *testing.T) {
	t.Run("add two items", func(t *testing.T) {
		pq := NewPqueue()
		pq.Enqueue(10, 1)
		pq.Enqueue(20, 2)
		if len(pq.items) != 2 {
			t.Error("expected 2 items in queue")
		}
		if pq.items[0].Priority != 2 {
			t.Error("expected priority 2 first")
		}
		if pq.items[1].Priority != 1 {
			t.Error("expected priority 1 second")
		}
	})

	t.Run("add two items reverse", func(t *testing.T) {
		pq := NewPqueue()
		pq.Enqueue(20, 2)
		pq.Enqueue(10, 1)
		if len(pq.items) != 2 {
			t.Error("expected 2 items in queue")
		}
		if pq.items[0].Priority != 2 {
			t.Error("expected priority 2 first")
		}
		if pq.items[1].Priority != 1 {
			t.Error("expected priority 1 second")
		}
	})
}

func TestDequeue(t *testing.T) {
	t.Run("dequeue empty queue", func(t *testing.T) {
		pq := NewPqueue()
		_, err := pq.Dequeue()
		if err == nil {
			t.Error("expected error on empty queue")
		}
	})

	t.Run("dequeue one item", func(t *testing.T) {
		pq := NewPqueue()
		pq.Enqueue(10, 1)
		item, err := pq.Dequeue()
		if err != nil {
			t.Error("unexpected error dequeuing item")
		}
		if item.Priority != 1 {
			t.Error("expected priority 1")
		}
		if item.Item != 10 {
			t.Error("expected item 10")
		}
		if len(pq.items) != 0 {
			t.Error("expected empty queue after dequeue")
		}
	})

	t.Run("dequeue multiple items", func(t *testing.T) {
		pq := NewPqueue()
		pq.Enqueue(10, 1)
		pq.Enqueue(20, 2)
		pq.Enqueue(30, 3)

		item, err := pq.Dequeue()
		if err != nil {
			t.Error("unexpected error dequeuing item")
		}
		if item.Priority != 3 {
			t.Error("expected highest priority (3) first")
		}
		if len(pq.items) != 2 {
			t.Error("expected 2 items remaining")
		}

		item, err = pq.Dequeue()
		if err != nil {
			t.Error("unexpected error dequeuing item")
		}
		if item.Priority != 2 {
			t.Error("expected priority 2 second")
		}
		if len(pq.items) != 1 {
			t.Error("expected 1 item remaining")
		}
	})
}

func BenchmarkPqueueDequeue(b *testing.B) {
	// Setup queue with 1000 random items outside benchmark
	pq := NewPqueue()
	counts := make(map[int]int)

	for i := 0; i < 100000; i++ {
		item := rand.Intn(1000)
		priority := rand.Intn(10)
		pq.Enqueue(item, priority)
	}

	b.Run("dequeue all items", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for {
				item, err := pq.Dequeue()
				if err != nil {
					break
				}
				counts[item.Priority]++
			}
		}
	})

	// Print priority counts
	for priority := 9; priority >= 0; priority-- {
		if count, exists := counts[priority]; exists {
			fmt.Printf("Priority %d: %d items\n", priority, count)
		}
	}
}
