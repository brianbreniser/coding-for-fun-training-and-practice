package queue

import (
	"testing"
)

func TestQueue(t *testing.T) {
	t.Run("Initial test", func(t *testing.T) {
		if 1 != 1 {
			t.Fail()
		}
	})
}

func TestQueueInit(t *testing.T) {
	t.Run("Creating a queue works", func(t *testing.T) {
		q := NewQueue()
		if q.items == nil {
			t.Fail()
		}
	})
}

func TestQueueSize(t *testing.T) {
	tests := []struct {
		name       string
		operations []int64
		wantLength int
	}{
		{
			name:       "Empty queue has length 0",
			operations: []int64{},
			wantLength: 0,
		},
		{
			name:       "Single item has length 1",
			operations: []int64{7},
			wantLength: 1,
		},
		{
			name:       "Two items has length 2",
			operations: []int64{7, 7},
			wantLength: 2,
		},
		{
			name:       "Three items has length 3",
			operations: []int64{7, 7, 1},
			wantLength: 3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := NewQueue()
			for _, op := range tt.operations {
				q.Add(op)
			}
			if got := q.Length(); got != tt.wantLength {
				t.Errorf("Queue.Length() = %v, want %v", got, tt.wantLength)
			}
		})
	}
}
