package queue

import (
	"fmt"
)

type Queue struct {
	items []int64
}

func NewQueue() Queue {
	return Queue{
		items: []int64{},
	}
}

func (q *Queue) Length() int {
	return len(q.items)
}

func (q *Queue) Add(x int64) {
	q.items = append(q.items, x)
}

func Doit() string {
	return fmt.Sprintf("Hello, World!")
}
