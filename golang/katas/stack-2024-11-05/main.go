package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello, World!")
}

type Stack struct {
	list []int
}

func NewStack() *Stack {
	return &Stack{
		list: []int{},
	}
}

func (s *Stack) Push(i int) {
	s.list = append(s.list, i)
}

func (s *Stack) Pop() int {
	if len(s.list) == 0 {
		return 0
	}
	i := s.list[len(s.list)-1]
	s.list = s.list[:len(s.list)-1]
	return i
}
