package main

import (
	"testing"
)

func TestMain(t *testing.T) {
	t.Run("Doit", func(t *testing.T) {
		if 1 != 1 {
			t.Fail()
		}
	})
}

func TestNewStack(t *testing.T) {
	t.Run("NewStack", func(t *testing.T) {
		stack := NewStack()
		if len(stack.list) != 0 {
			t.Fail()
		}
	})
}

func TestPush(t *testing.T) {
	tests := []struct {
		name   string
		inputs []int
	}{
		{"Add single number", []int{1}},
		{"Add multiple numbers", []int{1, 2, 3, 4, 5}},
		{"Add zero", []int{0}},
		{"Add negative", []int{-1}},
		{"Add large number", []int{999999}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			stack := NewStack()
			for _, input := range test.inputs {
				stack.Push(input)
			}
			if len(stack.list) != len(test.inputs) {
				t.Fail()
			}
		})
	}
}

func TestPop(t *testing.T) {
	tests := []struct {
		name   string
		inputs []int
	}{
		{"Pop single number", []int{1}},
		{"Pop multiple numbers", []int{1, 2, 3, 4, 5}},
		{"Pop zero", []int{0}},
		{"Pop negative", []int{-1}},
		{"Pop large number", []int{999999}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			stack := NewStack()
			for _, input := range test.inputs {
				stack.Push(input)
			}
			for i := len(test.inputs) - 1; i >= 0; i-- {
				if stack.Pop() != test.inputs[i] {
					t.Fail()
				}
			}
		})
	}
}
