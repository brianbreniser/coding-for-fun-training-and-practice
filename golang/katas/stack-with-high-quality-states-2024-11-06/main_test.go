package main

import (
	"testing"
)

func TestMain(t *testing.T) {
	main()
}

func TestStackBasics(t *testing.T) {
	s := NewStack()
	if len(s.data) != 0 {
		t.Error("New stack should be empty")
	}

	s.Push(1)
	if len(s.data) != 1 {
		t.Error("Stack should have length 1 after push")
	}
	if s.data[0] != 1 {
		t.Error("Stack should contain pushed value")
	}

	s.Push(2)
	if len(s.data) != 2 {
		t.Error("Stack should have length 2 after second push")
	}

	val, err := s.Pop()
	if err != nil {
		t.Error("Should not get error popping from non-empty stack")
	}
	if val != 2 {
		t.Errorf("Pop should return most recently pushed value, got %d", val)
	}
	if len(s.data) != 1 {
		t.Error("Stack started with 2 and we removed 1, should have length 1 after pop")
	}

	val, err = s.Pop()
	if err != nil {
		t.Error("Should not get error popping from non-empty stack")
	}
	if val != 1 {
		t.Errorf("Pop should return remaining value, got %d", val)
	}
	if len(s.data) != 0 {
		t.Error("Stack should be empty after popping all values")
	}

	val, err = s.Pop()
	if err == nil {
		t.Error("Should get error when popping from empty stack")
	}
	if stackError, ok := err.(StackError); ok {
		if stackError.Error() != "Stack is empty" {
			t.Error("Should get message 'Stack is empty'")
		}
	} else {
		t.Error("Should be able to cast error to StackError")
	}
}
