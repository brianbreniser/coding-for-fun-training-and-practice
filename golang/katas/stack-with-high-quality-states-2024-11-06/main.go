package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello, World!")
}

type Stack struct {
	data []int
}

func NewStack() *Stack {
	return &Stack{
		data: []int{},
	}
}

func (s *Stack) Push(n int) {
	s.data = append(s.data, n)
}

func (s *Stack) Pop() (int, error) {
	if len(s.data) == 0 {
		return 0, NewStackError(EmptyStack, "Stack is empty")
	}
	n := s.data[len(s.data)-1]
	s.data = s.data[:len(s.data)-1]
	return n, nil
}

func (s *Stack) State() State {
	if len(s.data) == 0 {
		return State{StackState: Empty, Size: 0}
	}
	return State{StackState: HasElements, Size: len(s.data)}
}

// ###################### Our states experiement ######################

type StackState int

const (
	Empty StackState = iota
	HasElements
)

type State struct {
	StackState StackState
	Size       int
}

type ErrorType int

const (
	EmptyStack ErrorType = iota
	Authentication
	Other
)

type StackError struct {
	Type    ErrorType
	Message string // optional
}

func (e StackError) Error() string {
	return e.Message
}

func NewStackError(t ErrorType, msg string) StackError {
	return StackError{Type: t, Message: msg}
}
