package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello world")

	q := InitQue()

	q.push(7)
}

type LinkedList struct {
	head *Node
}

type Node struct {
	value int
	next *Node
}

func InitLinkedList() LinkedList {
	return LinkedList{
		head: nil,
	}
}

func (ll *LinkedList) push(x int) {
	if ll.head == nil {
		ll.head = &Node{
			value: x,
			next: nil,
		}
	}

	cur := ll.head
	for cur.next != nil {
		cur = cur.next
	}

	cur.next = &Node{
		value: x,
		next: nil,
	}
}

func (ll *LinkedList) len() (int, error) {
	if ll.head == nil {
		return 0, fmt.Errorf("Linked List has not been initialized yet")
	}

	count := 0

	cur := ll.head
	for cur.next != nil {
		count++
		cur = cur.next
	}

	return count, nil
}


type Que struct {
	items []int
}

func InitQue() Que {
	return Que{
		items: []int{},
	}
}

func (q *Que) push(x int) {
	q.items = append(q.items, x)
}

func (q *Que) pop() (int, error) {
	if len(q.items) == 0 {
		return 0, fmt.Errorf("No more items in this thing")
	}

	index := len(q.items) - 1
	ret := q.items[index]

	q.items = q.items[:index]

	return ret, nil
}

