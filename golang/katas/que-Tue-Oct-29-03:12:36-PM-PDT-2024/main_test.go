package main

import (
	"testing"
)

func contains(slice []int, item int) bool {
	for _, element := range slice {
		if element == item {
			return true
		}
	}

	return false
}

func TestManyQue(t *testing.T) {
	t.Run("Making a que works", func(t *testing.T) {
	})

	tests := []struct {
		name string
		items []int
	}{
		{"Finds 7 after entering it", []int{0}},
		{"Finds 7 after entering it", []int{7, 8, 9, 10}},
		{"Finds 7 after entering it", []int{7, 8, 9, 10}},
		{"Finds 7 after entering it", []int{7, 8, 9, 10}},
		{"Finds 7 after entering it", []int{7, 8, 9, 10}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := InitQue()
			for _, i := range tt.items {
				q.push(i)
			}
			for _, i := range tt.items {
				if !contains(q.items, i) {
					t.Errorf("Did not find a number after adding it to the queue: %d", i)
				}
			}
		})
	}
}

func TestManyPop(t *testing.T) {
	tests := []struct {
		name string
		shouldError bool
		fillWith []int
		mustReturn int
	}{
		{"errors on empty queue", true, []int{}, 0},
		{"errors on empty queue", false, []int{6, 8, 10}, 10},
		{"errors on empty queue", false, []int{6, 8, 10, 11}, 11},
		{"errors on empty queue", false, []int{6, 8, 5}, 5},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := InitQue()
			for _, i := range tt.fillWith {
				q.push(i)
			}

			ret, err := q.pop()
			if tt.shouldError && err == nil {
				t.Errorf("Expected an error, but did not get one")
			}

			if !tt.shouldError && err != nil {
				t.Errorf("Did not expect an error, but got one")
			}

			if ret != tt.mustReturn {
				t.Errorf("Expected %d, but got %d", tt.mustReturn, ret)
			}
		})
	}

}

func TestLinkedListLength(t *testing.T) {
	t.Run("errors on empty linked list", func(t *testing.T) {
		ll := InitLinkedList()
		_, err := ll.len()
		if err == nil {
			t.Errorf("Expected an error, but did not get one")
		}
	})

	tests := []struct {
		name string
		items []int
		length int
	}{
		{"Finds 7 after entering it", []int{7}, 1},
		{"Finds 7 after entering it", []int{7, 8, 9, 10}, 4},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ll := InitLinkedList()
			for _, i := range tt.items {
				ll.push(i)
			}

			ret, err := ll.len()
			if err != nil {
				t.Errorf("Did not expect an error, but got one")
			}

			if ret != tt.length {
				t.Errorf("Expected %d, but got %d", tt.length, ret)
			}
		})
	}
}

// Benchmark adding 1000 items to a linked list
func BenchmarkLinkedListPush(b *testing.B) {
	ll := InitLinkedList()
	for i := 0; i < b.N; i++ {
		for j := 0; j < 1000; j++ {
			ll.push(i)
		}
	}
}

// Benchmark adding 1000 items to the queue
func BenchmarkQuePush(b *testing.B) {
	q := InitQue()
	for i := 0; i < b.N; i++ {
		for j := 0; j < 1000; j++ {
			q.push(i)
		}
	}
}

