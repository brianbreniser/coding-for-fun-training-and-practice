package queue

import (
	"fmt"
)

type Queue struct {
	items []int
}

func NewQueue() *Queue {
	return &Queue{
		items: []int{},
	}
}

func (q *Queue) Enqueue(i int) {
	q.items = append(q.items, i)
}

func (q *Queue) Dequeue() (int, error) {
	if len(q.items) == 0 {
		return 0, fmt.Errorf("queue is empty")
	}
	toRemove := q.items[0]
	q.items = q.items[1:]
	return toRemove, nil
}

func (q *Queue) Length() int {
	return len(q.items)
}
