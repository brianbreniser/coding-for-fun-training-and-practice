package queue

import (
	"testing"
)

func TestNewQueue(t *testing.T) {
	q := NewQueue()
	if q == nil {
		t.Errorf("NewQueue() returned nil")
	}
}

func TestEnqueue(t *testing.T) {
	q := NewQueue()
	q.Enqueue(1)
	if q.Length() != 1 {
		t.Errorf("Queue size should be 1 after enqueuing one item")
	}

	q.Enqueue(2)
	if q.Length() != 2 {
		t.Errorf("Queue size should be 2 after enqueuing two items")
	}

	q.Enqueue(3)
	if q.Length() != 3 {
		t.Errorf("Queue size should be 3 after enqueuing three items")
	}
}

func TestDequeue(t *testing.T) {
	q := NewQueue()
	q.Enqueue(1)
	q.Enqueue(2)
	q.Enqueue(3)

	val1, err := q.Dequeue()
	if err != nil {
		t.Errorf("Error dequeuing first value: %v", err)
	}
	if val1 != 1 {
		t.Errorf("First dequeued value should be 1, got %v", val1)
	}

	val2, err := q.Dequeue()
	if err != nil {
		t.Errorf("Error dequeuing second value: %v", err)
	}
	if val2 != 2 {
		t.Errorf("Second dequeued value should be 2, got %v", val2)
	}

	val3, err := q.Dequeue()
	if err != nil {
		t.Errorf("Error dequeuing third value: %v", err)
	}
	if val3 != 3 {
		t.Errorf("Third dequeued value should be 3, got %v", val3)
	}
}
