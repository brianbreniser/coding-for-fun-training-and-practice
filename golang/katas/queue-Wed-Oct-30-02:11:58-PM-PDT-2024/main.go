package main

import (
	"fmt"
	"math/rand"
	"os"
	priorityqueue "queue/priority_queue"
	"queue/queue"
	"strconv"
	"time"
)

func main() {
	fmt.Println("Hello, World!")
	fmt.Println("Change")

	q := queue.NewQueue()
	pq := priorityqueue.NewPriorityQueue()

	var seed int64
	seed = 0
	seedIndex := -1
	for i := 1; i < len(os.Args); i++ {
		if os.Args[i] == "-s" {
			seedIndex = i
			break
		}
	}

	if seedIndex != -1 && seedIndex+1 < len(os.Args) {
		seedVal, err := strconv.ParseInt(os.Args[seedIndex+1], 10, 64)
		if err == nil {
			seed = seedVal
		}
	}

	if seed == 0 {
		seed = time.Now().UnixNano()
	}

	rand.New(rand.NewSource(seed))
	fmt.Printf("Random seed: %d\n", seed)

	for i := 0; i < 1; i++ {
		action := rand.Int()
		if action%2 == 0 {
			// Enqueue
			value := rand.Int()
			q.Enqueue(value)
		} else {
			// Dequeue
			value, err := q.Dequeue()
			if err != nil {
				fmt.Printf("Error dequeue %d: %v\n", value, err)
			}
			if err == nil {
				fmt.Printf("Dequeued: %d\n", value)
			}
		}
	}

	insert_order := []priorityqueue.QueueItem{}

	for i := 0; i < 10; i++ {
		priority := rand.Intn(10)
		data := rand.Intn(100)

		insert_order = append(insert_order, priorityqueue.NewQueueItem(priority, data))
		pq.Enqueue(priority, data)
	}

	fmt.Println("Inserted order: ", insert_order)
	pq.Pp()

	for i := 0; i < 10; i++ {
		fmt.Println("Dequeued: ", pq.Dequeue())
	}

}
