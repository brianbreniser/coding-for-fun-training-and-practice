package priorityqueue

import (
	"fmt"
)

type QueueItem struct {
	priority int
	value    int
}

func NewQueueItem(priority int, value int) QueueItem {
	return QueueItem{
		priority: priority,
		value:    value,
	}
}

type PriorityQueue struct {
	items []QueueItem
}

func (pq *PriorityQueue) Pp() {
	fmt.Println("{")
	fmt.Println("  \"items\": [")
	for i, item := range pq.items {
		if i == len(pq.items)-1 {
			fmt.Printf("    {\"priority\": %d, \"value\": %d}\n", item.priority, item.value)
		} else {
			fmt.Printf("    {\"priority\": %d, \"value\": %d},\n", item.priority, item.value)
		}
	}
	fmt.Println("  ]")
	fmt.Println("}")
}

func NewPriorityQueue() PriorityQueue {
	return PriorityQueue{
		items: make([]QueueItem, 0),
	}
}

func (pq *PriorityQueue) Length() int {
	return len(pq.items)
}

func (pq *PriorityQueue) Enqueue(priority int, value int) {
	fmt.Println("############ Running Enqueue")
	qi := QueueItem{priority, value}
	if pq.Length() <= 0 {
		pq.items = append(pq.items, qi)
		return
	}

	for i, item := range pq.items {
		fmt.Println("##### Item priority: ", item.priority, "Incoming priority: ", priority)
		if priority < item.priority {
			before := pq.items[:i]
			after := pq.items[i:]
			after = append([]QueueItem{qi}, after...)
			pq.items = append(before, after...)
			return
		}
	}

	pq.items = append(pq.items, qi)
}

func (pq *PriorityQueue) Dequeue() QueueItem {
	if pq.Length() == 0 {
		return QueueItem{}
	}

	item := pq.items[0]
	pq.items = pq.items[1:]
	return item
}
