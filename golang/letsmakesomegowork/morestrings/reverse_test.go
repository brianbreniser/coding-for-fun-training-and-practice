package morestrings

import "testing"

func TestReverseRunes(t *testing.T) {
    cases := []struct {
        in, want string
    }{
        {"Hello, world", "dlrow ,olleH"},
        {"Hello, 世界", "界世 ,olleH"},
        {"", ""},
    }

    for _, c := range cases {
        got := ReverseRunes(c.in)

        if got != c.want {
            t.Errorf("Reversing the rune %q, expected %q, got %q", c.in, c.want, got)
        }
    }
}

