package main

import (
    "fmt"
    "hello/morestrings"
    "github.com/google/go-cmp/cmp"
)

func main() {
    s := "Hello, World!"
    fmt.Println(s)
    fmt.Println(morestrings.ReverseRunes(s))
    fmt.Println(cmp.Diff("Hello World", "Hello Go"))
}

