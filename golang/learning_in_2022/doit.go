package main

import (
	"fmt"
	//"io.ioutil"
	m "math"
	//"net/http"
	//"os"
	//"strconv"
)

func main() {
	fmt.Println("Hello world!")

	beyondHello()
	one()
	two()
	three()
	fmt.Println("")
}

func beyondHello() {
	var x int
	x = 7
	y := 9
	fmt.Println("x:", x, "y:", y)
	sum, prod := mtp(x, y)
	fmt.Println("Sum:", sum, "Prod:", prod)

	s := "string type"
	fmt.Println("string:", s)
	var ui uint = 7
	fmt.Println("also works:", float32(ui))
	next()
	slice()
}

func mtp(x, y int) (sum, prod int) {
	return x + y, x * y
}

func next() {
	var arrayx [4]int
	arrayx[0] = 7
	arrayx[3] = 11
	a1 := arrayx // copy, separate instances
	a1[1] = 9

	fmt.Println("The array:", arrayx)
	fmt.Println("copy a1:", a1)
}

func learnMemory() (p, q *int) {
	p = new(int)
	s := make([]int, 20)
	s[3] = 7
	r := -2
	return &s[3], &r
}

func slice() {
	s3 := []int{1, 2, 3, 4}
	bs := []byte("a slice")

	fmt.Println("slice:", s3)
	fmt.Println("slice of bytes of \"a slice\":", bs)

	s3 = append(s3, 9, 8, 7)
	fmt.Println("slice:", s3)

	s := []int{5, 6, 4, 7, 78, 8}
	s3 = append(s3, s...)
	fmt.Println("slice:", s3)

	p, q := learnMemory()
	fmt.Println("p, q:", *p, *q)
}

type Stringer interface {
    String() string
}

type pair struct {
    x, y int
}

func (p pair) String() string {
    return fmt.Sprintf("(%d, %d)", p.x, p.y)
}

func one() {
    fmt.Println(m.Exp(10))

    for key, value := range map[string]int{"one": 1, "two": 2, "three": 3} {
        fmt.Printf("key=%s, value=%d\n", key, value)
    }


}

func two() {
}

func three() {
}
