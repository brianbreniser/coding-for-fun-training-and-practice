package main

import (
	"fmt"
)

type LLM struct {
	name             string
	url              string
	promtEngineering string
}

func (llm *LLM) setName(name string) {
	llm.name = name
}

func (llm *LLM) setUrl(url string) {
	llm.url = url
}

func (llm *LLM) setPromptEngineerig(pe string) {
	llm.promtEngineering = pe
}

func (l LLM) String() string {
	return fmt.Sprintf("\n{\n\tllm name: %s,\n\tllm url: %s,\n\tllm pe: %s\n}", l.name, l.url, l.promtEngineering)
}

func editLLM(llm *LLM, name string, url string, pe string) {
	llm.name = name
	llm.url = url
	llm.promtEngineering = pe
}

func main() {
	fmt.Println("Hello, World!")

	llm := LLM{
		name: "the name",
		url: "url",
		promtEngineering: "you are an awesome ai",
	}

	fmt.Printf("The llm: %s\n\n", llm)

	llm.setName("new name")
	llm.setUrl("new url")
	llm.setPromptEngineerig("new pe")

	fmt.Printf("The llm: %s\n\n", llm)

	llmobj := &LLM{
		name: "the name",
		url: "url",
		promtEngineering: "you are an awesome ai",
	}

	fmt.Printf("The second llm: %s\n\n", llmobj)

	// asyncronously edit the llmobj, use a channel to wait for the edit to be done so we can print it
	done := make(chan string)
	go func() {
		editLLM(llmobj, "NEW name", "NEW url", "NEW pe")
		done <- "done"
	}()
	result := <-done

	if result == "done" {
		fmt.Printf("The second llm: %s\n\n", llmobj)
	} else {
		fmt.Println("The result was: ", result)
	}

	next()
}

func next() {
	items := 1000
	done := make(chan bool)
	for i := 0; i < items; i++ {
		go func(i int) {
			fmt.Printf(" %d ", i)
			done <- true
		}(i)
	}

	for i := 0; i < items; i++ {
		<-done
	}

	fmt.Println("All done")

}

