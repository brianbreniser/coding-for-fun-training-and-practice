package main

import (
	"fmt"
	"log"
	"math"
	"net"
	"net/url"
	"os"
	"reflect"
	"time"
	"unicode/utf8"
	//"io"
	//"bufio"
)

func split(message ...string) {
	fmt.Println("")
	if len(message) > 0 {
		fmt.Println("===", message)
	} else {
		fmt.Println("===")
	}
	fmt.Println("")
}

func main() {
	fmt.Println("Begin =====")
	fmt.Println("go " + "hello world")
	fmt.Println("1+1 =", 1+1)
	fmt.Println("1.0+1.0 =", 1.0+1.0)
	fmt.Println("7.0/3.0 =", 7.0/3.0)
	fmt.Println(!true)

	split()

	var a = "initial"
	fmt.Println(a)

	var b, c = 1, 2
	fmt.Println(b, c)

	d := 7
	fmt.Println(d)

	e, f := 9, 10 // Weird but works
	fmt.Println(e, f)

	split()

	// const big = 1000000000000000000000000000000000000000000000000000
	// const bigger = 100000000000000000000000000000000000000000000000000000
	// fmt.Println(big)
	const n = 500000000
	const m = 3e20 / n
	fmt.Println(m)

	fmt.Println(int64(m))

	fmt.Println(math.Sin(n))

	next()
}

func next() {
	split()

	i := 1
	for i <= 3 {
		fmt.Println(i)
		i = i + 1
	}

	split()

	for j := 7; j <= 9; j++ {
		fmt.Println(j)
	}

	split()

	for {
		fmt.Println("Inf loop end with 'break'")
		break
	}

	split()

	for n := 0; n <= 5; n++ {
		if n%2 != 0 {
			continue
		}
		fmt.Println(n)
	}

	split()

	if 7%2 == 0 {
		fmt.Println("7 is even")
	} else {
		fmt.Println("7 is odd")
	}

	split()

	next1()
}

func next1() {
	split()

	i := 2
	fmt.Print("Write", i, " as ")
	switch i {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("two")
	case 3:
		fmt.Println("three")
	}

	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("It's the weekend")
	default:
		fmt.Println("It's a weekday")
	}

	split()

	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("It's before Noon")
	default:
		fmt.Println("It's after Noon")
	}

	whatAmI := func(i interface{}) {
		switch t := i.(type) {
		case bool:
			fmt.Println("I'm a bool")
		case int:
			fmt.Println("I'm a int")
		default:
			fmt.Printf("Don't know type %T\n", t)
		}
	}

	whatAmI(true)
	whatAmI(17)
	whatAmI("Blah")

	fmt.Println(reflect.TypeOf("zcxv"))

	arrays()
}

func arrays() {
	split()

	var a [5]int
	fmt.Println("emp:", a)

	a[4] = 100
	fmt.Println("update:", a)
	fmt.Println("get:", a[4])

	fmt.Println("leng:", len(a))

	b := [5]int{1, 2, 3, 4, 5}
	fmt.Println("dcl:", b)

	var twoD [2][3]int
	for i := 0; i < 2; i++ {
		for j := 0; j < 3; j++ {
			twoD[i][j] = i + j
		}
	}

	fmt.Println("2d:", twoD)

	slices()
}

func slices() {
	split()

	s := make([]string, 3)
	fmt.Println(s)

	s[0] = "a"
	s[1] = "b"
	s[2] = "c"
	fmt.Println(s)
	fmt.Println(s[2])
	fmt.Println(len(s))

	s = append(s, "d")
	s = append(s, "e", "f")
	fmt.Println(s)
	fmt.Println(s[2])
	fmt.Println(len(s))

	c := make([]string, len(s))
	copy(c, s)
	fmt.Println("copy:", c)

	l := s[2:5]
	fmt.Println("sli:", l)

	l = s[:5]
	fmt.Println("sli:", l)

	l = s[2:]
	fmt.Println("sli:", l)

	t := []string{"g", "h", "i"}
	fmt.Println("new:", t)

	split()

	twoD := make([][]int, 3)
	fmt.Println("2d:", twoD)

	for i := 0; i < 3; i++ {
		innerLen := i + 1
		twoD[i] = make([]int, innerLen)
		for j := 0; j < innerLen; j++ {
			twoD[i][j] = i + j
		}
	}

	fmt.Println("2d:", twoD)

	maps()
}

func maps() {
	split()

	m := make(map[string]int)

	m["k1"] = 7
	m["k2"] = 13
	fmt.Println("map:", m)

	v1 := m["k1"]
	fmt.Println("map:", v1)
	fmt.Println("len:", len(m))

	delete(m, "k2")
	fmt.Println("map:", v1)
	fmt.Println("len:", len(m))

	x, prs := m["k1"]
	fmt.Println("k1:", x)
	fmt.Println("prs:", prs)

	n := map[string]int{"foo": 1}
	fmt.Println("map:", n)

	doit_range()
}

func doit_range() {
	split()

	nums := []int{2, 3, 4}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println("sum:", sum)

	for i, num := range nums {
		if num == 3 {
			fmt.Println("Found 3 at index:", i)
		}
	}

	kvs := map[string]string{"a": "apple", "b": "bannana"}
	for k, v := range kvs {
		fmt.Printf("%s -> %s\n", k, v)
	}
	fmt.Println("kvs:", kvs)

	for k := range kvs {
		fmt.Println("key:", k)
	}

	for i, c := range "go" {
		fmt.Println(i, c)
	}

	fn()
}

func plus(a int, b int) int {
	return a + b
}

func plusplus(a, b, c int) int {
	return a + b + c
}

func vals() (int, int) {
	return 3, 7
}

func sum(nums ...int) {
	fmt.Print(nums, " sum to ")
	total := 0

	for _, num := range nums {
		total += num
	}

	fmt.Println(total)
}

func fn() {
	split()

	fmt.Println(plus(7, 1))
	fmt.Println(plusplus(7, 1, 2))
	a, b := vals()
	fmt.Println(a, b)

	sum(1, 2, 3, 4, 45, 5)
	nums := []int{9, 8, 7, 6, 5, 4, 3, 2, 1}
	sum(nums...)

	closures()
}

func intSeq() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func fact(n int) int {
	if n == 0 {
		return 1
	}
	return n * fact(n-1)
}

func closures() {
	split("closures")

	nextInt := intSeq()

	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())

	newInts := intSeq()
	fmt.Println(newInts())
	fmt.Println(newInts())
	fmt.Println(newInts())

	fmt.Println(fact(20))

	var fib func(n int) int

	fib = func(n int) int {
		if n < 2 {
			return n
		}

		return fib(n-1) + fib(n-2)
	}

	fmt.Println(fib(20))

	point()
}

func zeroval(ival int) {
	ival = 0
}

func zeroptr(iptr *int) {
	*iptr = 0
}

func point() {
	split("pointers")

	i := 1
	fmt.Println("initial:", i)

	zeroval(i)
	fmt.Println("zeroval:", i)

	zeroptr(&i)
	fmt.Println("zeroptr:", i)

	fmt.Println("pointer:", &i)

	str()
}

func examineRune(r rune) {
	if r == 't' {
		fmt.Println("found tee")
	} else if r == 'ส' {
		fmt.Println("found so sua")
	}

}

func str() {
	split("Strings and runes")

	const s = "สวัสดี"

	fmt.Println("len:", len(s))

	for i := 0; i < len(s); i++ {
		fmt.Printf("%x ", s[i])
	}
	fmt.Println()

	fmt.Println("Rune count:", utf8.RuneCountInString(s))

	for idx, runeValue := range s {
		fmt.Printf("%#U starts at %d\n", runeValue, idx)
	}

	fmt.Println("\nUsing DecodeRuneInString")
	for i, w := 0, 0; i < len(s); i += w {
		runeValue, width := utf8.DecodeRuneInString(s[i:])
		fmt.Printf("%#U starts at %d\n", runeValue, i)
		w = width

		examineRune(runeValue)
	}

	doit_structs()
}

type person struct {
	name string
	age  int
}

func newPerson(name string) *person {
	p := person{name: name}
	p.age = 42
	return &p
}

func doit_structs() {
	split("Structs")

	fmt.Println(person{"bob", 20})
	fmt.Println(person{name: "alice", age: 20})
	fmt.Println(person{name: "bob"})
	fmt.Println(&person{name: "ann", age: 21})
	fmt.Println(newPerson("bob"))

	s := person{name: "sean", age: 50}
	fmt.Println(s.name)

	sp := &s
	fmt.Println(sp.age)

	sp.age = 51
	fmt.Println(sp.age)
	fmt.Println(s.age)

	meth()
}

type rect struct {
	width, height int
}

func (r *rect) area() int {
	return r.width * r.height
}

func (r *rect) perimiter() int {
	return 2*r.width + 2*r.height
}

func meth() {
	split("Methods")

	r := rect{width: 10, height: 100}
	fmt.Println("Area:", r.area())
	fmt.Println("Peremiter:", r.perimiter())

	do_interfaces()
}

type geometry interface {
	area() float64
	perim() float64
}

type rect64 struct {
	width, height float64
}

type circle struct {
	radius float64
}

func (r rect64) area() float64 {
	return r.width * r.height
}

func (r rect64) perim() float64 {
	return 2*r.width + 2*r.height
}

func (c circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

func (c circle) perim() float64 {
	return 2 * math.Pi * c.radius
}

// ===

func measure(g geometry) {
	fmt.Println("type:", reflect.TypeOf(g))
	fmt.Println("item:", g)
	fmt.Println("area:", g.area())
	fmt.Println("perim:", g.perim())
	fmt.Println()
}

func do_interfaces() {
	split("Interfaces")

	r := rect64{width: 3, height: 4}
	c := circle{radius: 5}

	measure(r)
	measure(c)

	neturl()
}

func neturl() {
	split("url")

	s := "postgres://user:pass@host.com:5432/path?k=v#f"

	u, err := url.Parse(s)
	if err != nil {
		panic(err)
	}

	fmt.Println(u.Scheme)
	fmt.Println(u.User)
	fmt.Println(u.User.Username())
	fmt.Println(u.Host)
	host, port, _ := net.SplitHostPort(u.Host)
	fmt.Println(host)
	fmt.Println(port)
	fmt.Println(u.Path)
	fmt.Println(u.Fragment)
	fmt.Println(u.RawQuery)
	m, _ := url.ParseQuery(u.RawQuery)

	fmt.Println(m)
	fmt.Println(m["k"][0])

	iofiles()
}

func check(e error) {
	if e != nil {
		log.Println("Error:", e.Error())
	}
}

func iofiles() {
	split("files")

	dat, err := os.ReadFile("/tmp/dat")
	check(err)
	fmt.Print(string(dat))

	do_go()
}

func doit(from string) {
	log.Println(from, ": ran doit")
}

func doit_long(from string) {
    for i := 0; i < 3; i++ {
	    fmt.Println(from, ": ran doit", i)
    }
}

func do_go() {
	split("go")

	//go doit_long("goroutine")
	//go doit_long("goroutine")
	//go doit_long("goroutine")

    //doit_long("Last")
    //time.Sleep(1 * time.Second)
    fmt.Println("Done")

    channels()
}

func channels() {
    split("Channels")

    messages := make(chan int, 3)

    go func() { messages <- 1 }()
    go func() { messages <- 2 }()
    go func() { messages <- 3 }()

    fmt.Println(<-messages)
    fmt.Println(<-messages)
    fmt.Println(<-messages)

    tm()
}

func tm() {
    split("Tm")

    //c1 := make(chan string, 1)
    //go func() {
        //time.Sleep(2 * time.Second)
        //c1 <- "result 1"
    //}()
//
    //select {
    //case res := <-c1:
        //fmt.Println(res)
    //case <- time.After(300 * time.Millisecond):
        //fmt.Println("timeout 1")
    //}
//
    //c2 := make(chan string, 1)
    //go func() {
        //time.Sleep(2 * time.Second)
        //c2 <- "result 2"
    //}()
//
    //select {
    //case res := <-c2:
        //fmt.Println(res)
    ////case <- time.After(3 * time.Second):
        //fmt.Println("timeout 2")
    //}

    signals()
}

func signals() {
    split("signals")

    messages := make(chan string)
    signals := make(chan bool)

    //select {
    //case msg := <-messages:
        //fmt.Println("Received message:", msg)
    //default:
        //fmt.Println("No messages received")
    //}
//
    //msg := "hi"
    //select {
    //case messages <- msg:
        //fmt.Println("sent message", msg)
    //default:
        //fmt.Println("no message sent")
    //}
//
    //select {
    //case msg := <-messages:
        //fmt.Println("received message", msg)
    //case sig := <-signals:
        //fmt.Println("received signal", sig)
    //default:
        //fmt.Println("no activity")
    //}

    close(messages)
    close(signals)
}
