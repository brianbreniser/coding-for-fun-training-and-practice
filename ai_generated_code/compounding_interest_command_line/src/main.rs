use rust_decimal::{Decimal, MathematicalOps};
use pico_args::Arguments;
use std::env;
use std::str::FromStr;

// Major TODO: This doesn't work! I could pull in my other work, but I was trying to make AI make
// this decision. I'm not sure how to do that yet, so I'm going to leave this as is for now.
// However, it did help me write this comment, so I'm going to leave it in.

#[derive(Debug)]
enum CompoundingRate {
    Monthly,
    Annually,
}

fn calculate_compound_interest(
    principal: Decimal,
    rate: Decimal,
    years: u32,
    contributions_per_month: Decimal,
    compounding_rate: CompoundingRate,
) -> Decimal {
    let (number_of_periods, contributions_per_period) = match compounding_rate {
        CompoundingRate::Monthly => (years * 12, contributions_per_month),
        CompoundingRate::Annually => (years, contributions_per_month * Decimal::from(12)),
    };

    if number_of_periods == 0 {
        return principal;
    }

    let rate_per_period = rate / Decimal::from(100) / Decimal::from(number_of_periods as i32);
    let total_periods = Decimal::from(number_of_periods as i32);

    let compound_factor = (Decimal::from(1) + rate_per_period).powd(total_periods);
    let future_value_contributions = (contributions_per_period / rate_per_period) * (compound_factor - Decimal::from(1));

    let future_value_principal = principal * compound_factor;
    future_value_principal + future_value_contributions
}


fn main() {
    let args = env::args_os().skip(1).collect::<Vec<_>>();
    let mut args = Arguments::from_vec(args);

    let principal: Decimal = args
        .value_from_str("--principal")
        .unwrap_or_else(|_| Decimal::from(0));
    let rate: Decimal = args
        .value_from_str("--rate")
        .unwrap_or_else(|_| Decimal::from_str("0").unwrap());
    let years: u32 = args
        .value_from_str("--years")
        .unwrap_or_else(|_| 0);
    let contributions_per_month: Decimal = args
        .value_from_str("--contributions")
        .unwrap_or_else(|_| Decimal::from(0));
    let compounding_rate: String = args
        .value_from_str("--compounding")
        .unwrap_or_else(|_| String::from("monthly"));

    let compounding_rate = match compounding_rate.as_str() {
        "monthly" => CompoundingRate::Monthly,
        "annually" => CompoundingRate::Annually,
        _ => panic!("Invalid compounding rate"),
    };

    let result = calculate_compound_interest(
        principal,
        rate,
        years,
        contributions_per_month,
        compounding_rate,
    );
    println!("Total amount after {} years: {:.2}", years, result);
}

