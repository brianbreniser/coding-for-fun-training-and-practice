from bs4 import BeautifulSoup

# The HTML content to be parsed
html_content = """
<html>
    <head>
        <title>Test HTML</title>
    </head>
    <body>
        <h1>Test HTML</h1>
        <p>This is a test HTML file.</p>
        <div class="test1">
            inside first div
            <custom class="test2">
                inside second div
            </custom>
            <custom class="test2">
                inside the second, second div
            </custom>
        </div>
    </body>
</html>
"""

# Parse the HTML content using Beautiful Soup
soup = BeautifulSoup(html_content, 'html.parser')

# Find the inner div element with class "test2"
inner_div = soup.find_all('custom', {'class': 'test2'})

# Print the contents of the inner div element
if inner_div is not None:
    for i in inner_div:
        print(i.text.strip())
else:
    print('No inner div found')

