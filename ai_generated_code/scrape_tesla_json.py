"""Scrape Tesla inventory data from their website."""
import json
import requests

###########################
# helper functions
###########################
def trim(arg):
    """Get the trim of the car."""
    return arg['TRIM'][0]

def extract_autopilot(arg):
    """Extract autopilot from the ADL_OPTS field."""
    autopilot = arg['AUTOPILOT']
    if autopilot is None:
        return "['NO AUTOPILOT']"

    return autopilot

###########################
# main
###########################

# pylint: disable=C0301
URL = 'https://www.tesla.com/inventory/api/v1/inventory-results?query={"query":{"model":"m3","condition":"used","options":{},"arrangeby":"Price","order":"asc","market":"US","language":"en","super_region":"north america","lng":-122.4979879,"lat":45.6585294,"zip":"98682","range":0,"region":"WA"},"offset":0,"count":50,"outsideOffset":0,"outsideSearch":false}'

response = requests.get(URL, timeout=10)
json_data = json.loads(response.text)

# Print the number of results and the name of the first car in the list

for i in json_data["results"]:
    print(f"{trim(i)}, ${i['Price']}, {i['Odometer']} Miles;\t {extract_autopilot(i)}{i['ADL_OPTS']}")
