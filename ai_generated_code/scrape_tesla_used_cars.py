import sys
import logging
from requests_html import HTMLSession

# Set up logging
logging.basicConfig(stream=sys.stderr, level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s')

# Create an HTML session object
session = HTMLSession()

# URL of the Tesla used inventory page
url = 'https://www.tesla.com/inventory/used/m3?arrangeby=plh&zip=98682'

# Use the HTML session object to get the page content and render the JavaScript
response = session.get(url)
logging.info('Rendering...')
response.html.render()
logging.info('Rendering Done!')

# Check if the request was successful
if response.status_code == 200:
    logging.info(f'Successfully connected to {url}')
else:
    logging.error(f'Failed to connect to {url}')

# Find all the cars listed on the page
cars = response.html.find('article.result.card')

logging.info('=====================================')
logging.info(f'Found {len(cars)} cars')
logging.info('=====================================')

# Loop through each car and extract its features
for car in cars:
    name = car.find('h3', first=True).text.strip()
    price = car.find('div.result-price', first=True).text.strip()
    range = car.find('div.tdx-text--h4', first=True).text.strip()

    # Print out the features of the car
    logging.info(f'Name: {name}')
    logging.info(f'Price: {price}')
    logging.info(f'Range: {range}')
    logging.info('')

# Close the HTML session
session.close()

logging.info('Scraping complete.')

