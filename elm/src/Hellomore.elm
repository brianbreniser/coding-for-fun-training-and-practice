module Hellomore exposing (main)

import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)

main =
    Browser.sandbox {init = 0, update = update, view = view}

type Msg = Increment | Decrement

update msg model =
    case msg of
        Increment -> 
            model + 1

        Decrement -> 
            model - 1
        
fib n =
    if n <= 2 then
        1
    else
        rfib n 1 1

rfib : Int -> Int -> Int -> Int
rfib n y z =
    if n == 0 then
        y
    else
        let
            count = n - 1
            temp = y + z
        in
            rfib count temp y

firstordefault : List Int -> Int
firstordefault aList =
    case aList of
        [] -> 1
        [x] -> 1
        x::xs -> x

inc = 
    button [ onClick Increment ] [ text "+"]

disp model =
    div [] [ text ( String.fromInt model ) ]

dec =
    button [ onClick Decrement ] [ text "-"]

appcomma : String -> String -> String
appcomma a b =
    a ++ ", " ++ b

view model =
    div []
        [ inc
        , disp model
        , dec
        , div []
            [ text "Fib of model is: "
            , text (String.fromInt (fib model))
            ]
        , div []
            [
                text (List.foldl (appcomma) ("") (List.map String.fromInt (List.reverse [1, 2, 3])))
            ]
        , div []
            [
                text "hi"
            ]
        ]
