fun main(args: Array<String>) {
    println("Hello World!")
}

fun staticthing(arg: Boolean): String =
    when(arg) {
        true -> "hi"
        false -> "bye"
        else -> "what?"
    }
