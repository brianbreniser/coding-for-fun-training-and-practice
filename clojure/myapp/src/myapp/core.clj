(ns myapp.core
  (:gen-class))

(defn square [x] (* x x))

(defn fib_recurse [two_last last count]
  (cond (= count 0)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "the square of 5 is: ")
  (println (square 5))
  (println)
  
)
