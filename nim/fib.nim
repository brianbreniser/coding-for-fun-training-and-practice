import macros
import times, os
import unittest
import bignum

macro `|>`(lhs, rhs: untyped): untyped =
  case rhs.kind:
    of nnkIdent: #single-parameter functions
      result = newCall(rhs, lhs)
    else:
      result = rhs
      result.insert(1, lhs)

proc bigFib(x: Int): Int =
  if x < 3:
    return newInt(1)

  var
    last, twolast: Int = newInt(1)

  for i in 3..x:
    let tmp: Int = last + twolast
    twolast = last
    last = tmp

  return last

proc fib(x: int): int =
  if x < 3:
    return 1

  var
    last, twolast: int = 1

  for i in 3..x:
    let tmp: int = last + twolast
    twolast = last
    last = tmp

  return last

# 1 1 2 3 5 8 13 21 34

test "fib 1":
  assert fib(1) == 1
  assert bigFib(newInt(1)) == 1

test "fib 2":
  assert fib(2) == 1
  assert bigFib(newInt(2)) == 1

test "fib 3":
  assert fib(3) == 2
  assert bigFib(newInt(3)) == 2

test "fib 4":
  assert fib(4) == 3
  assert bigFib(newInt(4)) == 3

test "fib 5":
  assert fib(5) == 5
  assert bigFib(newInt(5)) == 5

test "fib 6":
  assert fib(6) == 8
  assert bigFib(newInt(6)) == 8



var time = cpuTime()
echo fib(92)
echo "Time taken: ", cpuTime() - time

time = cpuTime()
# echo bigFib(newInt(1_000_000)) # can be done in 5.5 seconds
echo bigFib(newInt(1_000))
echo "Time taken: ", cpuTime() - time

