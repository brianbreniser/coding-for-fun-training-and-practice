import htmlgen
import jester
import strformat
import json

routes:
  get "/":
    resp h1("Hello world")
  get "/hello/@name?":
    if @"name" == "":
      halt "Please enter a name"
    else:
      resp "Hello " & @"name", "text/json"
  get "/debug":
    resp fmt"{request}"
  post "/":
    var push = parseJson(@"payload")
    resp "I got some JSON: " & $push

