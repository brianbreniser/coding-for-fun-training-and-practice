import os

proc isEven(x: int): bool =
  return x mod 2 == 0

proc calc_pi_iters(n: int): float =
  var r = 1.float

  for i in 0..(n-1):
    let x = 3 + 2*i

    if isEven(i + 1):
      r += 1/x
    else:
      r -= 1/x

  return r*4

for i in 0..1000:
  echo calc_pi_iters(i)
  sleep(50)

