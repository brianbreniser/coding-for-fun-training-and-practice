#from strutils import parseInt
import sequtils#, sugar
import times, os

# this line
echo "==========================="
echo "Hello"
let time = cpuTime()

discard 1 > 2
type
  Animal* = object
    name*, species*: string
    age: int

  Qwerminal = object
    name, species: string
    age: int

proc sleep*(a: var Animal) =
  a.age += 1

proc dead*(a: Animal): bool =
  result = a.age > 20

var carl: Animal
carl = Animal(name: "Carl",
              species: "L. glama",
              age: 12)

let joe = Animal(name: "Joe",
                 species: "H. sapiens",
                 age: 23)

let localanimal = Qwerminal(name: "local",
                           species: "still works",
                           age: 99)

assert(not carl.dead)

echo(carl)
echo(joe)
echo(localanimal)

type
  Name = string
  Age = int
  Person = tuple[name: Name, age: Age]
  qwer = tuple
    f1: Name
    f2: Age
  Cash = distinct int

let letter: char = 'n'
let boat: float = 7.0
let account: Cash = 100.Cash

const p: Person = (name: "brian", age: 34)

echo("A person: ", p)
echo("Cash in bank ", account.int.float)

echo(letter, " ", boat)

# echo "Give me money"
# case readLine(stdin)
# of "no", "NO", "No":
  # echo "Well fine"
# of "yes", "YES", "Yes":
  # echo "Awesome, well if it's that easy..."
  # echo "How much"
  # let newmoney = readLine(stdin)
  # let updatedAccount: Cash = (account.int + parseInt(newmoney)).Cash
  # echo "Now you have: ", updatedAccount.int
# else:
  # echo "Say something normal"

proc double(x: int): int =
  return x*2

var singles = toSeq(1..10)
var doubles = singles.map(double)

echo ("singles: ", singles)
echo ("doubles: ", doubles)

echo ""
echo "------------------"
echo "starting timers"
echo "------------------"
echo ""

echo "Time taken: ", cpuTime() - time
echo getTIme()

echo ""
echo "------------------"
echo "ending timers"
echo "------------------"
echo ""

echo "Goodbye"
echo "==========================="
