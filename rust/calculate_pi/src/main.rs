use std::{thread, time};

fn main() {
    let real_pi = "3.1415926535897932";
    let mut pi = 3f64;
    let mut s = 2i64;

    let mut count = 0i64;

    loop {
        count = count+1;
        pi = iternate(pi, s);
        s = s+4;

        let check_in = vec!{0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 700, 1000, 1500, 2000, 3000, 4000};

        if check_in.contains(&count) {
            let vec_real_pi: Vec<char> = real_pi.chars().collect();
            let vec_pi: Vec<char> = pi.to_string().chars().collect();

            thread::sleep(time::Duration::from_millis(500));
            erase_lines();

            println!("stats: s: {}, count: {}", s, count);
            println!("{}", real_pi);
            print!("{}", pi);
            println!();

            for (i, c) in vec_pi.iter().enumerate() {
                if c == &vec_real_pi[i] {
                    print!(" ");
                }
                else {
                    print!("x");
                }
            }
            println!();
        }

        if count > 4_000 {
            break;
        }

    }
}

// Side effect driven, prints escape sequences to terminal
fn erase_lines() {
    let erase_to_end = "\x1B[K";
    let move_cursor_to_previous_line = "\x1B[F";

    for _i in 0..4 {
        print!("{}{}", move_cursor_to_previous_line, erase_to_end);
    }
}

fn iternate(pi: f64, s: i64) -> f64{
    let s = s as f64;
    return pi + (4f64/((s)*(s+1f64)*(s+2f64)) - (4f64/((s+2f64)*(s+3f64)*(s+4f64))))
}
