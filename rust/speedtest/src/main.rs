#![feature(test)]

use rayon::prelude::*;
use std::mem;

extern crate test;
const PI: f32 = 3.1415926;

use std::hint::black_box;

use std::time::Instant;
use rand;

fn main() {
    let elapsted_time_1 = black_box(with_rayon());

    println!();
    println!("=== Test 1");
    println!("Time: {}", elapsted_time_1);

    let elapsted_time_2 = black_box(with_boxes());

    println!();
    println!("=== Test 2");
    println!("Time: {}", elapsted_time_2);

    println!();
    println!("=== Results");
    println!("Speedup: {:.2}", elapsted_time_1 as f64 / elapsted_time_2 as f64);

    /*
    println!("Addendum: size of test is {}", mem::size_of_val(&Square::new()));
    println!("Addendum: size of test is {}", mem::size_of_val(&Rectangle::new()));
    println!("Addendum: size of test is {}", mem::size_of_val(&Triangle::new()));
    println!("Addendum: size of test is {}", mem::size_of_val(&Circle::new()));

    let list: Vec<Box::<dyn Area>> = vec![
        Box::new(Square::new()),
        Box::new(Rectangle::new()),
        Box::new(Triangle::new()),
        Box::new(Circle::new()),
    ];
    println!("Addendum: size of test is {}", mem::size_of_val(&list));
    list.iter().for_each(|i| {
        println!("Addendum: size of test is {}", mem::size_of_val(&i));
    });
    */
}

#[allow(dead_code)]
fn slow_with_traits_and_random() -> u128 {
    let now = Instant::now();

    let s = Square::new();
    s.area();
    let r = Rectangle::new();
    r.area();
    let t = Triangle::new();
    t.area();
    let c = Circle::new();
    c.area();

    return now.elapsed().as_nanos();
}

#[allow(dead_code)]
fn fast_with_traits_and_static() -> u128 {
    let now = Instant::now();

    let s = Square { length: 10.0 };
    s.area();
    let s = Square { length: 10.0 };
    s.area();
    let s = Square { length: 10.0 };
    s.area();
    let s = Square { length: 10.0 };
    s.area();
    let s = Square { length: 10.0 };
    s.area();
    let s = Square { length: 10.0 };
    s.area();
    let s = Square { length: 10.0 };
    s.area();
    let r = Rectangle { length: 10.0, width: 10.0 };
    r.area();
    let t = Triangle { length: 10.0, width: 10.0 };
    t.area();
    let c = Circle {radius: 10.0};
    c.area();

    return now.elapsed().as_nanos();
}

#[allow(dead_code)]
fn with_boxes() -> u128 {
    let now = Instant::now();

    let list = [0; 1_000_000];

    list.par_iter()
        .map(|_| {
            Box::new(Square { length: 10.0 })
        })
        .for_each(|i| {
            i.area();
        });

    return now.elapsed().as_nanos();
}

#[allow(dead_code)]
fn with_rayon() -> u128 {
    let now = Instant::now();

    let list = [0; 1_000_000];

    list.par_iter()
        .map(|_| {
            Square { length: 10.0 }
        })
        .for_each(|i| {
            i.area();
        });

    return now.elapsed().as_nanos();
}

#[allow(dead_code)]
fn with_vector_traits_static() -> u128 {
    let now = Instant::now();

    let mut s = vec![];

    for _ in 1..=1_000_000 {
        s.push(Square { length: 10.0 });
    }

    for i in s {
        i.area();
    }

    return now.elapsed().as_nanos();
}

trait Area {
    fn area(&self) -> f32;
}

#[derive(Debug, Clone)]
struct Square {
    length: f32,
}

#[derive(Debug, Clone)]
struct Rectangle {
    length: f32,
    width: f32,
}

#[derive(Debug, Clone)]
struct Triangle {
    length: f32,
    width: f32,
}

#[derive(Debug, Clone)]
struct Circle {
    radius: f32,
}


impl Area for Square {
    fn area(&self) -> f32 {
        return self.length * self.length;
    }
}

#[allow(dead_code)]
impl Square {
    fn new() -> Square {
        Square {
            length: rand::random::<f32>()
        }
    }
}

impl Area for Rectangle {
    fn area(&self) -> f32 {
        return self.length * self.width;
    }
}

#[allow(dead_code)]
impl Rectangle {
    fn new() -> Rectangle {
        Rectangle {
            length: rand::random::<f32>(),
            width: rand::random::<f32>(),
        }
    }
}

impl Area for Triangle {
    fn area(&self) -> f32 {
        return 0.5f32 * self.length * self.width;
    }
}

#[allow(dead_code)]
impl Triangle {
    fn new() -> Triangle {
        Triangle {
            length: rand::random::<f32>(),
            width: rand::random::<f32>(),
        }
    }
}

impl Area for Circle {
    fn area(&self) -> f32 {
        return PI * self.radius * self.radius;
    }
}

#[allow(dead_code)]
impl Circle {
    fn new() -> Circle {
        Circle { 
            radius: rand::random::<f32>(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    /*
    #[test]
    fn test() {
        let s = Square { length: 10.0 };
        assert_eq!(s.area(), 100.0);
    }

    #[bench]
    fn bench_area(b: &mut Bencher) {
        b.iter(|| {
            let s = Square { length: 10.0 };
            s.area();
            let r = Rectangle { length: 10.0, width: 10.0 };
            r.area();
            let t = Triangle { length: 10.0, width: 10.0 };
            t.area();
            let c = Circle {radius: 10.0};
            c.area();
        });
    }

    #[bench]
    fn bench_random_area(b: &mut Bencher) {
        b.iter(|| {
            let s = Square::new();
            s.area();
            let r = Rectangle::new();
            r.area();
            let t = Triangle::new();
            t.area();
            let c = Circle::new();
            c.area();
        });
    }
    */

    /*
    #[bench]
    fn test_vector(b: &mut Bencher) {
        b.iter(||{
            with_vector_traits_static();
        });
    }
    */

    #[bench]
    fn test_vector(b: &mut Bencher) {
        b.iter(||{
            black_box(with_vector_traits_static());
        });
    }

    #[bench]
    fn test_rayon(b: &mut Bencher) {
        b.iter(||{
            black_box(with_rayon());
        });
    }
}

