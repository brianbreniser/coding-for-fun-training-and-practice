#![allow(unused)]

use std::io;
use std::thread;
use std::time::Duration;
use std::io::Write;
use std::future;

use futures::executor::block_on;

fn main() {
    doitwrapper();
}

fn doitwrapper() {
    block_on(doit());
}

async fn doit() {
    let mut f1 = doitasync(1);
    let mut f2 = doitasync(2);
    let mut f3 = doitasync(3);
    let mut f4 = doitasync(4);
    let mut f5 = doitasync(5);
    let mut f6 = doitasync(6);

    futures::join!(f1, f2, f3, f4, f5, f6);

    println!("Done with doit");
}

async fn doitasync(n: i32) {
    thread::sleep(Duration::from_secs(1));
    println!("Done with doitasync {}", n);
}
