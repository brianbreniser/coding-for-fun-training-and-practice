fn main() {
    println!("Start section 1");
    println!();

    let effeciencies = vec![2.5, 3.0, 3.5, 4.0];

    // Realistic right now charging speeds
    #[allow(unused_mut)]
    let mut charging_speed = vec![1.5, 6.6, 40.0, 50.0, 120.0, 150.0, 200.0, 250.0, 350.0, 500.0];

    // // Unrealistic charging speeds
    // let mut unrealistic_charging_speed = vec![600.0, 700.0, 800.0, 900.0 1000.0];
    // charging_speed.append(&mut unrealistic_charging_speed);

    // Simple charging times
    // let charging_times = vec![m(1), m(2), m(3), m(4), m(5), m(10), m(15), m(30), m(45), 1.0, 1.5, 2.0, 2.5, 3.0];

    // Complicated charging times
    let mut charging_times = vec![];
    for i in 1..16 { // 16 is the first number NOT used
        charging_times.push(m(i));
    }
    // for i in 5..30 {
    //     if i % 5 == 0 {
    //         charging_times.push(m(i));
    //     }
    // }
    // charging_times.append(&mut vec![0.5, 1.0, 6.0, 8.0, 10.0]);

    println!("effic\trate\ttime\trange\tbattery\tspeed & drive time");

    for e in &effeciencies {
        for s in &charging_speed {
            for t in &charging_times {
                let battery = s * t * 0.8;
                let miles = miles_of_charge(*e, *s, *t);
                let time = float_to_time(*t);
                let speed = 70.0;
                let hours = miles / speed;
                let drive_time = float_to_time(hours);

                // if battery >= 30.0 && battery <= 100.0 {
                if battery <= 100.0 && *t <= m(15) && hours >= 1.0 {
                    // println!("ef: {}\tsp: {}\tt: {}\tgained: {:.1}\tmiles ({:.1})\t@ {:.01}kwh\tdrive time @ {}mph: {}; ", e, s, time, miles, battery, speed, drive_time);
                    println!(
                        "{}\t{} kw\t{}\t{:.1}\t{:.0} kwh\t@ {}mph: {}; ",
                        e, s, time, miles, battery, speed, drive_time
                    );
                }
            }
        }
    }

    println!();

    println!();
    println!("End section 1");
}

fn m(min: i64) -> f64 {
    return minutes_to_hours(min);
}

fn minutes_to_hours(min: i64) -> f64 {
    return min as f64 / 60f64;
}

fn miles_of_charge(efficency: f64, speed: f64, time: f64) -> f64 {
    let charge_efficiency_rate = 0.8;
    return efficency * speed * time * charge_efficiency_rate;
}

fn float_to_time(time: f64) -> String {
    let hours = time as i32;
    let minutes: i32 = ((time - hours as f64) * 60_f64) as i32;

    return format!("{:02}:{:02}", hours, minutes);
}

/*
Efficiency (M/KWh)
Miles per hour (mph)
Charge rate (kw)
Charge time (seconds)
power loss when charging (0-1)
time able to drive (seconds)
*/
#[allow(dead_code)]
struct Stats {
    efficiency: f64,
    speed: f64,
    charge_rate: f64,
    charge_time: u64,
    power_loss: f64,
    drive_time: u64,
}

impl Stats {
    #[allow(dead_code)]
    fn calculate_drive_time(&mut self) {
        let battery_size = self.charge_rate * self.charge_time as f64;
        let range = battery_size * self.efficiency;

        let time = range / self.speed;

        self.drive_time = time as u64;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn minutes_to_hours_test() {
        assert_eq!(1.0, minutes_to_hours(60));
        assert_eq!(0.5, minutes_to_hours(30));
        assert_eq!(0.25, minutes_to_hours(15));
        assert_eq!(0.03333333333333333, minutes_to_hours(2));
        assert_eq!(0.016666666666666666, minutes_to_hours(1));
    }

    #[test]
    fn miles_of_charge_1_1_1() {
        let ret = miles_of_charge(1.0, 1.0, 1.0);

        assert_eq!(ret, 1.0 * 0.8);
    }

    #[test]
    fn miles_of_charge_3_1_025() {
        let ret = miles_of_charge(3.0, 1.0, 0.25);

        assert_eq!(ret, (3.0 * 1.0 * 0.25 * 0.8));
    }

    #[test]
    fn miles_of_charge_4_16_8() {
        let ret = miles_of_charge(4.0, 1.6, 8.0);

        assert_eq!(ret, (4.0 * 1.6 * 8.0 * 0.8));
    }

    #[test]
    fn all_miles_charge() {
        for e in 0..5 {
            for s in 1..250 {
                for t in 0..10 {
                    assert_eq!(
                        miles_of_charge(e as f64, s as f64, t as f64),
                        (e as f64 * s as f64 * t as f64 * 0.8)
                    );
                }
            }
        }
    }
}
