use orbtk::prelude::*;

fn main() {
    orbtk::initialize();

    Application::new()
        .window(|ctx| {
            Window::new()
                .title("Hello orbtk")
                .position((1600.0, 1100.0))
                .size(100.0, 150.0)
                .active(false)
                .resizeable(true)
                .child(HelloWorld::new().build(ctx))
                .child(MyButtonPlus::new().build(ctx))
                .build(ctx)
        })
        .run();
}

widget!(MyButtonPlus {});

impl Template for MyButtonPlus {
    fn template(self, id: Entity, ctx: &mut BuildContext) -> Self {
        self.child(
            Button::new()
                .text("Add 1")
                .on_enter(|_,_| {println!("Button entered"); })
                .on_leave(|_,_| {println!("Button left"); })
                .on_mouse_down(|_,_| { println!("Button mouse down"); true })
                .on_mouse_up(|_,_| { println!("Button mouse up"); })
                .on_click(move |ctx, _| {
                    ctx.send_message(CounterAction::Increment, id);
                    true
                })
                .build(ctx)
        )
    }
}

widget!(HelloWorld {});

impl Template for HelloWorld {
    fn template(self, _id: Entity, ctx: &mut BuildContext) -> Self {
        self.child(
            TextBlock::new()
                .text("Hello, world")
                .v_align("center")
                .h_align("center")
                .build(ctx)
        )
    }
}

#[derive(Debug, Default, AsAny)]
struct CounterState {
    count: i32,
}

impl State for CounterState {
    fn messages(
        &mut self,
        mut messages: MessageReader,
        _registry: &mut Registry,
        _ctx: &mut Context,
    ) {
        for message in messages.read::<CounterAction>() {
            match message {
                CounterAction::Increment => {
                    self.count += 1;
                }
            }
        }
    }
}


#[derive(Clone, Debug)]
enum CounterAction {
    Increment,
    // Decrement
}

widget!(
    InteractiveView<CounterAction> {
        count_text: String
    }
);

impl Template for InteractiveView
