import math
const (
    hello = 'hello'
    age = 42
)


// single line commit
fn main() {
    println("Hello world")
    println("${hello}, ${age}")

    streets := ['1234 asdf qwer', '0987 ;lkj ;lkj']
    address := Address {
        street: streets[0]
        city: 'beta'
        state: 'gamma'
        zip: 2345
    }

    println("Street address: ${address}")

    address2 := make_new_address('asdf', 'qwer', 'oregon', 1234)
    println("Street address: ${address2}")

    mut p := Point {
        x: 10
        y: 15
    }

    println("point: ${p}")
    println("point: ${p.pow()}")

    p.x = 7
    println("point: ${p.pow()}")
}

struct Point {
pub:
    x int
    y int
}

fn (p Point) str() string {
    return 'Point($p.x, $p.y)'
}

fn (p Point) pow() f64 {
    return math.pow(p.x, p.y)
}

fn make_new_address(s string, c string, st string, z int) Address {
    return Address {
        street: s
        city: c
        state: st
        zip: z
    }
}

struct Address {
pub:
    street  string
    city    string
    state   string
    zip     int
}

fn (a Address) str() string {
    return 'Address($a.street, $a.city, $a.state, $a.zip)'
}
